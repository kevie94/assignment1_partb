
/**
 * Created by Kevin on 26/09/2016.
 */
public class Registration {
    public static void main(String[] args) {

        Course course = new Course("4BCT", 2016, 8, 1, 2017, 5, 31);

        Module module1 = new Module("Software Engineering", "CT417");
        Module module2 = new Module("Artificial Intelligence", "CT421");
        Module module3 = new Module("Machine Learning & Data Mining", "CT475");

        Student student1 = new Student("Kevin", 22, "12520327");
        Student student2 = new Student("Tim", 21, "12420325");
        Student student3 = new Student("Alice", 23, "126405327");
        Student student4 = new Student("Matthew", 22, "12720532");
        Student student5 = new Student("David", 21, "122204323");
        Student student6 = new Student("Caroline", 21, "123203527");
        Student student7 = new Student("Noel", 23, "12542603432");

        course.addModule(module1);
        course.addModule(module2);
        course.addModule(module3);

        module1.addStudent(student1);
        module1.addStudent(student2);
        module1.addStudent(student3);
        module1.addStudent(student5);
        module1.addStudent(student7);

        module2.addStudent(student1);
        module2.addStudent(student2);
        module2.addStudent(student4);
        module2.addStudent(student6);
        module2.addStudent(student7);

        module3.addStudent(student1);
        module3.addStudent(student4);
        module3.addStudent(student5);
        module3.addStudent(student7);
        course.printAll();
    }
}
